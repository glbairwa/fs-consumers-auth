package com.scratchpad;

import com.scholastic.iam.sps.util.SchBlowfishCodec;

public class TestDecoder {

	public TestDecoder() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		//String pw = SchBlowfishCodec.singleton().decodeText("E38Su5erMfU=");
		String pw = SchBlowfishCodec.singleton().decodeText("RtuE//FawrqfUOpAZpgacA==\n");
		
		System.out.println("Found user nancykelley, pw=" + pw);
		
		pw = SchBlowfishCodec.singleton().encodeText("84704783");
		System.out.println("Encoded value="+pw);
		
		pw = SchBlowfishCodec.singleton().encodeText("Passw0rd");
		System.out.println("Encoded value="+pw);
		
		/*
		pw = SchBlowfishCodec.singleton().decodeText("E38Su6erMfU=");
		System.out.println("Found user nancykelley, pw=" + pw);
		pw = SchBlowfishCodec.singleton().decodeText("E38Ss5erMfU=");
		System.out.println("Found user nancykelley, pw=" + pw);
		pw = SchBlowfishCodec.singleton().decodeText("E38Tu5erMfU=");
		System.out.println("Found user nancykelley, pw=" + pw);
		pw = SchBlowfishCodec.singleton().decodeText("E38Sv5erMfU=");
		System.out.println("Found user nancykelley, pw=" + pw);
		pw = SchBlowfishCodec.singleton().decodeText("E33Su5erMfU=");
		System.out.println("Found user nancykelley, pw=" + pw);
		pw = SchBlowfishCodec.singleton().decodeText("E38Su5erMKU=");
		System.out.println("Found user nancykelley, pw=" + pw);
		*/
	}

}
