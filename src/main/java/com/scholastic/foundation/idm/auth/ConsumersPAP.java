package com.scholastic.foundation.idm.auth;

import java.util.Map;

import com.iplanet.sso.SSOToken;
import com.sun.identity.authentication.spi.AMPostAuthProcessInterface;
import com.sun.identity.authentication.spi.AuthenticationException;
import com.sun.identity.shared.debug.Debug;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConsumersPAP implements AMPostAuthProcessInterface {
    private final static String DEBUG_FILE = "ConsumersPAP";

    protected Debug debug = Debug.getInstance(DEBUG_FILE);

    public void onLoginSuccess(
            Map requestParamsMap,
            HttpServletRequest request,
            HttpServletResponse response,
            SSOToken token
    ) throws AuthenticationException {
    }

    public void onLoginFailure(
            Map requestParamsMap,
            HttpServletRequest request,
            HttpServletResponse response
    ) throws AuthenticationException {
        // Not used
    }

    public void onLogout(
            HttpServletRequest request,
            HttpServletResponse response,
            SSOToken token
    ) throws AuthenticationException {
    	if (debug.messageEnabled())  debug.message("onLogout() called");
    	Cookie[] cookies = request.getCookies();  
    	String domain = getDomain(request);    	
    	if (debug.messageEnabled())  debug.message("domain="+domain);
    	if ( cookies != null ) {
	    	for(int i = 0; i< cookies.length ; ++i){
	    	    if(cookies[i].getName().equals("SPS_SESSION")){
	    	    	if (debug.messageEnabled())  debug.message("Cookie ="+cookies[i].getName()+" value="+cookies[i].getValue()+ " Domain="+cookies[i].getDomain()+" Path=" +cookies[i].getPath()+" MaxAge="+cookies[i].getMaxAge());
	    	        cookies[i].setValue("");        
	    	        cookies[i].setPath("/");
	    	        cookies[i].setDomain(domain);
	    	        cookies[i].setMaxAge(0);        
	    	        response.addCookie(cookies[i]);
	    	        if (debug.messageEnabled())  debug.message("SPS_SESSION  removed");
	    	    }
	    	    if(cookies[i].getName().equals("SPS_TSP")){
	    	    	if (debug.messageEnabled())  debug.message("Cookie ="+cookies[i].getName()+" value="+cookies[i].getValue()+ " Domain="+cookies[i].getDomain()+" Path=" +cookies[i].getPath()+" MaxAge="+cookies[i].getMaxAge());
	    	        cookies[i].setValue("");
	    	        cookies[i].setPath("/");
	    	        cookies[i].setDomain(domain);
	    	        cookies[i].setMaxAge(0);
	    	        response.addCookie(cookies[i]);
	    	        if (debug.messageEnabled())  debug.message("SPS_TSP  removed");
	    	    }
	    	    if(cookies[i].getName().equals("SPS_UD")){
	    	    	if (debug.messageEnabled())  debug.message("Cookie ="+cookies[i].getName()+" value="+cookies[i].getValue()+ " Domain="+cookies[i].getDomain()+" Path=" +cookies[i].getPath()+" MaxAge="+cookies[i].getMaxAge());
	    	        cookies[i].setValue("");
	    	        cookies[i].setPath("/");
	    	        cookies[i].setDomain(domain);
	    	        cookies[i].setMaxAge(0);
	    	        response.addCookie(cookies[i]);
	    	        if (debug.messageEnabled())  debug.message("SPS_UD  removed");
	    	    }
	    	    if(cookies[i].getName().equals("XUS_EI")){
	    	    	if (debug.messageEnabled())  debug.message("Cookie ="+cookies[i].getName()+" value="+cookies[i].getValue()+ " Domain="+cookies[i].getDomain()+" Path=" +cookies[i].getPath()+" MaxAge="+cookies[i].getMaxAge());
	    	        cookies[i].setValue("");
	    	        cookies[i].setPath("/");
	    	        cookies[i].setDomain(domain);
	    	        cookies[i].setMaxAge(0);
	    	        response.addCookie(cookies[i]);
	    	        if (debug.messageEnabled())  debug.message("XUS_EI  removed");
	    	    }
	    	} 
    	}
    	if (debug.messageEnabled())  debug.message("====================================="); 
    	if (debug.messageEnabled())  debug.message("Logged out successfully.");   
    }
    
    private String getDomain(HttpServletRequest request) {
    	String serverName = request.getServerName();
    	String domain = serverName;
    	int lastIndex = serverName.lastIndexOf('.');
    	if (lastIndex != 1  ) {
    		int lastToLastIndex = serverName.substring(0, lastIndex).lastIndexOf('.');
    		if ( lastToLastIndex != -1  )
    		domain = serverName.substring(lastToLastIndex);
    	}
    	return domain;
    }
}