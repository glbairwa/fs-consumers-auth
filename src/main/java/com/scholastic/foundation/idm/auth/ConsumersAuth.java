/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2013 ForgeRock AS. All Rights Reserved
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://forgerock.org/license/CDDLv1.0.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at http://forgerock.org/license/CDDLv1.0.html
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 */

package com.scholastic.foundation.idm.auth;

import java.security.Principal;
import java.util.Map;
import java.util.ResourceBundle;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.forgerock.opendj.ldap.ConnectionException;

import com.scholastic.iam.sps.SPSException;
import com.scholastic.iam.sps.service.UserService;
import com.scholastic.iam.sps.session.SPSSessionManager;
import com.scholastic.iam.sps.session.SpsUser;
import com.scholastic.iam.sps.util.SchBlowfishCodec;
import com.sun.identity.authentication.spi.AMLoginModule;
import com.sun.identity.authentication.spi.AuthLoginException;
import com.sun.identity.authentication.spi.InvalidPasswordException;
import com.sun.identity.authentication.util.ISAuthConstants;
import com.sun.identity.shared.datastruct.CollectionHelper;
import com.sun.identity.shared.debug.Debug;



public class ConsumersAuth extends AMLoginModule
{

    // Name for the debug-log
    private final static String DEBUG_NAME = "ConsumersAuth";

    // Name of the resource bundle
    private final static String amAuthConsumersAuth = "amAuthConsumersAuth";

    // User names for authentication logic
    private final static String USERNAME = "demo";
    private final static String ERROR_NAME_NOT_FOUND = "error-name-not-found";
    private final static String ERROR_BAD_CREDENTIALS = "error-bad-credentials";

    // Orders defined in the callbacks file
    private final static int STATE_BEGIN = 1;
    private final static int STATE_AUTH = 2;
    private final static int STATE_ERROR = 3;

    private final static Debug debug = Debug.getInstance(DEBUG_NAME);

    private Map options;
    private ResourceBundle bundle;
    private SpsUser authenticatedUser = null;

    private final UserService userService = new UserService();


    public ConsumersAuth()
    {
        super();
    }



    @Override
    // This method stores service attributes and localized properties
    // for later use.
    public void init(Subject subject, Map sharedState, Map options)
    {
        if (debug.messageEnabled())
        {
            debug.message("ConsumersAuth::init");
        }
        this.options = options;
        bundle = amCache.getResBundle(amAuthConsumersAuth, getLoginLocale());
    }



    @Override
    public int process(Callback[] callbacks, int state) throws LoginException
    {

        if (debug.messageEnabled())
        {
            debug.message("ConsumersAuth::process state: " + state);
        }

        SpsUser spsUser = null;
    	HttpServletRequest httpRequest = getHttpServletRequest();
    	HttpServletResponse httpResponse = getHttpServletResponse();
    	

        switch (state)
        {

            case STATE_BEGIN:
            	// check if the user has already authenticated (via the SPS cookie)
            	try {
            		spsUser = SPSSessionManager.getUserBySession(httpRequest, null);
            	}
            	catch (SPSException se) {
            		// error getting the sps user
            		 if (debug.messageEnabled())  debug.message("Error retrieving the user's session", se);
            	}
            	
            	
            	if (null != spsUser) {
            		if  (debug.messageEnabled()) debug.message("Logging in based on SPS session cookie");
            		//extend SPS session. 
            		//TODO: ensure the session time out is same for for SPS and OpenAM
            		SPSSessionManager.extendSession(httpResponse, httpRequest);
            		//
            		this.authenticatedUser = spsUser;
            		return ISAuthConstants.LOGIN_SUCCEED;
            	}
            	
                // No time wasted here - simply modify the UI and
                // proceed to next state
                substituteUIStrings();
                return STATE_AUTH;

            case STATE_AUTH:
            try {
                // Get data from callbacks. Refer to callbacks XML file.
                NameCallback nc = (NameCallback) callbacks[0];
                PasswordCallback pc = (PasswordCallback) callbacks[1];
                String username = nc.getName();
                String password = new String(pc.getPassword());

                spsUser = userService.getUserByUsername(username);
                if (null==spsUser) {
                	if (debug.messageEnabled()) debug.message("Error: Could not get user by name");
                    setErrorText(ERROR_NAME_NOT_FOUND);
                    return STATE_ERROR;
                }
                
                
                String decryptedPw = SchBlowfishCodec.singleton().decodeText(spsUser.getPassword());
                //TODO: remove below line after dev phase is over.
                //System.out.println("Found! username=" + spsUser.getUsername() + ", password=" + spsUser.getPassword());
                            
                if (password.equals(decryptedPw))
                {
                	this.authenticatedUser = spsUser;                	
                	// set the SPS cookies to indicate this is an authenticated user
                	SPSSessionManager.loginUser(spsUser.getSpsId(),  httpRequest, httpResponse, false);
                	SPSSessionManager.placeUDCookie(httpRequest, httpResponse, spsUser);
                	if (debug.messageEnabled()) debug.message("Success: Login succeeded");
                    return ISAuthConstants.LOGIN_SUCCEED;
                }

                throw new InvalidPasswordException("password is wrong", USERNAME);
            }
            catch (ConnectionException e) {
            	debug.error("Error: ConnectionException. Could not connect to LDAP.");
            	e.printStackTrace();
            	throw new LoginException("Internal server error");
            }
            catch (Exception e) {
            	debug.error("Error:"+e.toString());
            	e.printStackTrace();
            	throw new LoginException("Internal server error");
            }

            case STATE_ERROR:
                return STATE_ERROR;
            default:
                throw new AuthLoginException("invalid state");

        }
    }



    @Override
    public Principal getPrincipal()
    {
    	if (debug.messageEnabled()) debug.message("getPrincipal: returning " + this.authenticatedUser.getUsername());
    	return new ConsumersAuthPrincipal(this.authenticatedUser.getUsername());
    }



    private void setErrorText(String err) throws AuthLoginException
    {
        // Receive correct string from properties and substitute the
        // header in callbacks order 3.
        substituteHeader(STATE_ERROR, bundle.getString(err));
    }



    private void substituteUIStrings() throws AuthLoginException
    {
        // Get service specific attribute configured in OpenAM
        String ssa = CollectionHelper.getMapAttr(options,
                "consumersauth-service-specific-attribute");

        // Get property from bundle
        String new_hdr = ssa + " "
                + bundle.getString("consumersauth-ui-login-header");
        substituteHeader(STATE_AUTH, new_hdr);

        Callback[] cbs_phone = getCallback(STATE_AUTH);

        replaceCallback(STATE_AUTH, 0, new NameCallback(bundle
                .getString("consumersauth-ui-username-prompt")));

        replaceCallback(STATE_AUTH, 1, new PasswordCallback(bundle
                .getString("consumersauth-ui-password-prompt"), false));
    }

    
    public static void main(String[] args) throws Exception {
//    	SpsUser spsUser = SampleAuth.searchUser("nancykelley");
//   	if (null != spsUser) {
    		String pw = SchBlowfishCodec.singleton().decodeText("E38Su5erMfU=");
    		System.out.println("Found user nancykelley, pw=" + pw);
//    	}
    }
}
