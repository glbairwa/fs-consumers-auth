package com.scholastic.iam.sps.service;

import org.forgerock.opendj.ldap.Connection;
import org.forgerock.opendj.ldap.LDAPConnectionFactory;
import org.forgerock.opendj.ldap.SearchScope;
import org.forgerock.opendj.ldap.responses.SearchResultEntry;
import org.forgerock.opendj.ldif.ConnectionEntryReader;
import org.forgerock.opendj.ldap.Attribute;

import com.scholastic.iam.sps.session.SpsUser;
import com.scholastic.iam.sps.util.SPSEnvProperties;
import com.scholastic.iam.sps.util.SchBlowfishCodec;

public class UserService {

	public UserService() {
	}

	
    private Connection getConnection() throws Exception {
    	SPSEnvProperties spsProps = SPSEnvProperties.getInstance();
    	String spsLdapHost = spsProps.getString("spsLdapHost"); //"10.21.14.16";
    	int spsLdapPort = spsProps.getInt("spsLdapPort");// 389;
    	String spsBindUser=spsProps.getString("spsBindUser"); // "cn=Directory Manager";
    	String spsBindPassword=spsProps.getString("spsBindPassword");// "Passw0rd";
    	//password is encrypted , so decrypt it first
    	spsBindPassword = SchBlowfishCodec.singleton().decodeText(spsBindPassword);
    	//
    	LDAPConnectionFactory factory = new LDAPConnectionFactory(spsLdapHost, spsLdapPort);
    	Connection connection = factory.getConnection();
		connection.bind(spsBindUser, spsBindPassword.toCharArray());
		return connection;
    }
    
    public SpsUser getUserByUsername(String username) throws Exception {
    	return getUserByCriteria("(cn=" + username + ")");
    }
    

    public SpsUser getUserBySpsId(String spsId) throws Exception {
    	String spsLdapUserDN = "spsid=" + spsId + ",ou=SPSUsers2,c=US,o=SPS";
    	
    	SpsUser spsUser = null;
    	
    	Connection connection = getConnection();
        SearchResultEntry entry = connection.readEntry(spsLdapUserDN);
        if (entry != null) {
        	spsUser = unmarshallUser(entry);
        }
        connection.close();
        return spsUser;
    }

    private SpsUser getUserByCriteria(String searchCriteria) throws Exception {
    	String spsLdapUserBaseDN = "ou=SPSUsers2,c=US,o=SPS";
    	
    	SpsUser spsUser = null;
    	
    	Connection connection = getConnection();
        ConnectionEntryReader reader = connection.search( spsLdapUserBaseDN, SearchScope.WHOLE_SUBTREE, searchCriteria);
        if (reader.hasNext()) {
        	spsUser = unmarshallUser(reader.readEntry());
        }
        connection.close();
        return spsUser;
    }
    
    private SpsUser unmarshallUser(SearchResultEntry entry) {
    	SpsUser spsUser = new SpsUser();
    	String username = getEntryAttribute(entry,"cn");
    	String password = getEntryAttribute(entry,"pwd");
    	String spsId = getEntryAttribute(entry,"spsid");
    	String firstName = getEntryAttribute(entry,"givenName");
    	String lastName = getEntryAttribute(entry,"sn");
    	String email = getEntryAttribute(entry,"semail");
    	String bcoeId = getEntryAttribute(entry,"bcoe");
    	spsUser = new SpsUser();
    	spsUser.setUsername(username);
    	spsUser.setPassword(password);
    	spsUser.setSpsId(spsId);
    	spsUser.setFirstName(firstName);
    	spsUser.setLastName(lastName);
    	spsUser.setEmail(email);
    	spsUser.setBcoeId(bcoeId);
    	return spsUser;
    }
    
    private String getEntryAttribute(SearchResultEntry entry, String key) {
    	Attribute attribute = entry.getAttribute(key);
    	if (null == attribute) {
    		return null;
    	}
    	return attribute.firstValueAsString();
    }
    
    public static final void main(String[] args) throws Exception {
    	UserService service = new UserService();
    	//SpsUser user = service.getUserBySpsId("84288020");
    	SpsUser user = service.getUserByUsername("glbairwa31@yahoo.com");
    	System.out.println("user.username=" + user.getUsername());
    	System.out.println("user.pwd=" + SchBlowfishCodec.singleton().decodeText(user.getPassword()));
    }
}
