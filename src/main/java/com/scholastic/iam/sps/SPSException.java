/*
 * Created on Oct 29, 2003
 * Modified on 06/01/2012 by Venkat Medarametla
 * 
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.scholastic.iam.sps;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.scholastic.util.SchStringUtils;

/**
 * @author redhat
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class SPSException extends Exception {
	private List errorsList = new ArrayList();
	private List errorsCodeList = new ArrayList();
	//private final static String ERROR_PROPS = "SPSExceptionErrors";
	private static ResourceBundle bundle ;
	private boolean isRawErrorMsg = false;
	private String errorCode = "";
	
	
	static{
		bundle = ResourceBundle.getBundle("SPSExceptionErrors");
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public List getErrorsCodeList() {
		return errorsCodeList;
	}

	public void setErrorsCodeList(List errorsCodeList) {
		this.errorsCodeList = errorsCodeList;
	}
	
	public SPSException(String error) {
		super(error);
		errorsList.add(error);
	}
	
	public SPSException(String error,boolean isRawErrorMsg){
		super(error);
		errorsList.add(error);
		this.isRawErrorMsg = isRawErrorMsg;
	}
	
	public boolean hasRawErrorMsg(){
		return isRawErrorMsg;
	}
	
	public SPSException(String error, Throwable cause) {
		super(error);
		errorsList.add(error);
	}
	
	public SPSException(int errorCode) {
		super();
		try{
			errorsList.add(bundle.getString(String.valueOf(errorCode)));
		}catch(Exception re){
			errorsList.add("NOT IMPLEMENTED FUNCTION");
		}
	}
	
	public SPSException(int errorCode, Throwable cause) {
		super();
		try{
			errorsList.add(bundle.getString(String.valueOf(errorCode)));
		}catch(Exception re){
			errorsList.add("NOT IMPLEMENTED FUNCTION");
		}
	}
	
	public SPSException(List errorsList) {
		super((String)errorsList.get(0));
		this.errorsList = errorsList;
	}
	
	public SPSException(List errorsList, Throwable cause) {
		super((String)errorsList.get(0)/*, cause*/);
		this.errorsList = errorsList;
	}
	
	public SPSException(int errorCode, String errorMsg) {
		super(errorMsg);
		try{
			errorsList.add(bundle.getString(String.valueOf(errorCode)));
			errorsList.add(errorMsg);
		}catch(Exception re){
			errorsList.add("NOT IMPLEMENTED FUNCTION");
		}
	}
	
	public void addError(int errorCode) {
		try{
			errorsList.add(bundle.getString(String.valueOf(errorCode)));
		}catch(Exception re){
			errorsList.add("NOT IMPLEMENTED FUNCTION");
		}
	}
	
	public void addError(String error) {
		errorsList.add(error);
	}
	
	public void addError(List error) {
		errorsList.addAll(error);
	}
	
	public boolean containsError(int errorCode) {
		try{
			return containsError(bundle.getString(String.valueOf(errorCode)));
		}catch(Exception re){
			return false;
		}
	}
	
	public boolean containsError(String error) {
		boolean hasError = false;
		if (!(hasError = errorsList.contains(error))){
			for (int i=0 ; i<errorsList.size(); ++i){
				String errorMsg = (String) errorsList.get(i);
				if(hasError =(!SchStringUtils.isEmpty(errorMsg)
						&& errorMsg.indexOf(error) != -1)){
					break;
				}
			}
		}
		return hasError;
	}
	
	public String generateErrorsParam() {
		return generateErrorsParam(errorsList);
	}
	
	public static String generateErrorsParam(List errorsList) {
		return generateErrorsParam(getErrorsPipeSeparated(errorsList));
	}
	
	public String getErrorsPipeSeparated() {
		return getErrorsPipeSeparated(errorsList);
	}
	public static String getErrorsPipeSeparated(List errorsList) {
		StringBuffer errors = new StringBuffer("");
		for(int i = 0; i < errorsList.size(); i++){
			if(i != 0)
				errors.append("|");
			errors.append(errorsList.get(i));
		}
		
		return errors.toString();
	}
	
	
	public static String generateErrorsParam(String errors) {
		return "errors=" + errors;
	}
	
	public List getErrors() {
		return errorsList;
	}

	public void setErrors(List errorsList) {
		this.errorsList = errorsList;
	}	

	public void clearErrors() {
		errorsList = new ArrayList();
	}
	public String toString() {
		return generateErrorsParam();
	}
}