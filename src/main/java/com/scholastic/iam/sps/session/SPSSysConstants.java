/*
 * Created on Oct 30, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.scholastic.iam.sps.session;


/**
 * @author redhat
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public interface SPSSysConstants extends SPSErrorConstants{

	public final static String DEFAULT_APP_ID = "SPS";
	public final static String APP_ID = "appid";
	public final static String DEFAULT_ENV_ID = "DEV";
	
	public final static String IDENTIFIER_KEY = "spsid";
	public final static String BASE_IDENTIFIER_KEY = "basespsid";
	public final static String SUB_IDENTIFIER_KEY = "subspsid";
	public final static String OBJECT_CLASS_KEY = "objectclass";
	//public final static String [] DN_KEY_PARAM = {"dn"};
	
	//transaction  types
	public static final int READ_ONLY_OPERATION = 1;
	public static final int READ_WRITE_OPERATION = 2;
	
	//search  types
	public final static int EXACT_SEARCH_TYPE = 0;
	public final static int STARTS_WITH_SEARCH_TYPE = 1;
	public final static int ENDS_WITH_SEARCH_TYPE = 2;
	public final static int CONTAINS_SEARCH_TYPE = 3;
	public final static int IN_SEARCH_TYPE = 4;
	public final static int EXACT_NOT_SEARCH_TYPE = 5;
	public final static int STARTS_NOT_WITH_SEARCH_TYPE = 6;
	public final static int ENDS_NOT_WITH_SEARCH_TYPE = 7;
	public final static int CONTAINS_NOT_WITH_SEARCH_TYPE = 8;
	public final static int DEFAULT_SEARCH_TYPE = EXACT_SEARCH_TYPE;
	
	//filter types
	public final static int AND_SEARCH_RULE = 0;
	public final static int OR_SEARCH_RULE = 1;
	public final static int ALL_OF_THE_ABOVE_AND = 2;
	public final static int ALL_OF_THE_ABOVE_OR= 3;
	public final static int DEFAULT_SEARCH_RULE = AND_SEARCH_RULE;
	
	

	//Object types
	public final static int LDAP_OBJECT_TYPE = 0;
	public final static int SQL_OBJECT_TYPE = 1;
	public final static int DUMMY_OBJECT_TYPE = 2;
	
	//Object names
	public final static String TOP_OBJECT_VALUE = "top";
	public final static String SEQUENCE_OBJECT_VALUE = "SPSSequenceKey";
	public final static String CONNECTION_OBJECT_VALUE = "SPSConnection";
	public final static String APP_DATA_OBJECT_VALUE = "SPSApplicationData";
	public final static String SPS_ENV_DATA_OBJECT_VALUE = "SPSEnvData";

	//misc
	public final static int SEQUENCE_CHUNK_SIZE = 100;
	public final static String US_SEARCH_LEVEL = "o=sps";	

	// needed by SPSWriteLdapEntity
	public final static String ENTITY_NAME = "ENTITY_NAME";
	public final static String WRITE_PROPERTIES = "WRITE_PROPERTIES";
	
	// needed by SPSWriteLdapEntityProperty
	public final static String LDAP_ENTITY_ID = "LDAP_ENTITY_ID";
	public final static String PROPERTY_NAME = "PROPERTY_NAME";
	
	// neded by SPSApplicationData
	public final static String WRITE_ENTITIES = "WRITE_ENTITIES";
	public final static String HAS_PARTIAL_ACCESS_KEY = "HAS_PARTIAL_ACCESS_KEY";

	// Vertex
	public final static String VTX_STATUS = "VTXStatus";
	public final static String VTX_TOTAL_TAX = "VTXTotalTax";
	public final static String VTX_SUB_TOTAL = "VTXSubTotal";
	public final static String VTX_TOTAL = "VTXTotal";
	public final static String VTX_TRANSACTIONID = "VTXTransactionId";
	public final static String VTX_RESPONSEXML = "VTXResponseXML";
	
	public final static String VTX_LINEITEM = "VTXLineItem";
	public final static String VTX_FREIGHTLINEITEM = "VTXFreightLineItem";
	public final static String VTX_LINEITEM_TOTAL_TAX = "VTXLineItemTotalTax";
	public final static String VTX_LINEITEM_ID = "VTXLineItemTId";
	public final static String VTX_LINEITEM_PRODUCT = "VTXLineItemProduct";
	public final static String VTX_LINEITEM_FREIGHTTOTALTAX = "VTXFreightTotalTax";
	
	public final static String PRIVACY_POLICY_VERSION_KEY = "PRIVACY_POLICY_VERSION_KEY";
	public final static String TERMS_CONDITIONS_VERSION_KEY = "TERMS_CONDITIONS_VERSION_KEY";
	public final static String COPPA_PRIVACY_POLICY_VERSION_KEY="COPPA_PRIVACY_POLICY_VERSION_KEY";
	
}
