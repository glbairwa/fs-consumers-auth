//
// SPSCookieHelper created by alexpod on  May 25, 2005
//
 
package com.scholastic.iam.sps.session;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.scholastic.iam.sps.util.SchBlowfishCodec;
import com.scholastic.util.SchStringUtils;

public class SPSCookieHelper {
	private static final Logger myLogger = Logger.getLogger("session");
	public static int THIRTY_DAYS = 2592000;

	public static boolean placeCookie(HttpServletRequest request, HttpServletResponse response, String cookieName, String value, boolean isTmp, boolean isEncrypted){
		int timeOut = -199;
		if(!isTmp)
			timeOut = THIRTY_DAYS;
		return placeCookie(request, response, cookieName, value, timeOut, isEncrypted);
	}
	
	public static boolean placeCookie(HttpServletRequest request, HttpServletResponse response, String cookieName, String value, int timeOut, boolean isEncrypted){
		if(isEncrypted)
			value = encryptValue(value);
		Cookie cookie = findCookie(request, cookieName);
		if(cookie != null)
			cookie.setValue(value);
		else
			cookie = new Cookie(cookieName, value);
		cookie.setPath("/");
		
		if(timeOut != -199)
			cookie.setMaxAge(timeOut);
		
		String domain = guessDomain(request);
		//TODO : Uncomment once dev phase is over
		//if(domain.indexOf("scholastic") != -1)
			cookie.setDomain(domain);
		
		response.addCookie(cookie);
		
		
		if(myLogger.isDebugEnabled())
			myLogger.debug("Placed " + cookieName + " cookie, cookie.getDomain() = " + cookie.getDomain() + ", value =  " + value );
		
		return true;
	}
	
	public static void killCookie(HttpServletRequest request, HttpServletResponse response, String cookieName){
         Cookie cookie = new Cookie(cookieName, "");
         cookie.setPath("/");
         cookie.setMaxAge(0);
         String domain = guessDomain(request);
         //TODO : Uncomment once dev phase is over
         //if(domain.indexOf("scholastic") != -1)
                 cookie.setDomain(domain);
         response.addCookie(cookie);
	}
	
	public static String getCookieValue(HttpServletRequest request, String cookieName, boolean isEncrypted){
		Cookie cookie = findCookie(request, cookieName);
		if(cookie == null){
			if(myLogger.isDebugEnabled())
				myLogger.debug("did not find cookie named: " + cookieName);
			return null;
		}
		
		String value = cookie.getValue();
		if(myLogger.isDebugEnabled()) myLogger.debug("Cookie Name="+cookieName+"Domain="+cookie.getDomain()+" Value="+value );
		if(SchStringUtils.isEmpty(value)){
			if(myLogger.isDebugEnabled())
				myLogger.debug("did not find value in the cookie: " + cookieName);
			return null;
		}
		
		if(isEncrypted)
			return decryptValue(value);
		else
			return value;
	}
	
	public static Cookie findCookie(HttpServletRequest request, String cookieName){
        Cookie[] cookies = request.getCookies();
        if(cookies == null || cookies.length == 0){
            if(myLogger.isDebugEnabled())
                myLogger.debug("No cookies found!!");
            return null;
        }

        for(int i = 0; i < cookies.length; i++){
            if(cookies[i].getName().equalsIgnoreCase(cookieName)){
                return cookies[i];
            }
        }
        return null;
	}
	
	public static String encryptValue(String value){
        if(myLogger.isDebugEnabled())
            myLogger.debug("Ecrypting: " + value);

        if(SchStringUtils.isEmpty(value)){
	        if(myLogger.isDebugEnabled())
                myLogger.debug("value is empty, returning...");
	        return value;
        }

        value = SchBlowfishCodec.singleton().encodeText(value);
        //TODO: WHY ?
        if(value != null)
                value = value.replace('\n',' ').trim();
        //
        return value;
    }

	public static String decryptValue(String value){
        if(myLogger.isDebugEnabled())
            myLogger.debug("decryptValue called, decoding: " + value);

        if(SchStringUtils.isEmpty(value)){
            if(myLogger.isDebugEnabled())
                myLogger.debug("value is empty, returning...");
            return null;
        }
        //TODO: Does not seem right.
        value+='\n';
        //
        value = SchBlowfishCodec.singleton().decodeText(value);
        if(myLogger.isDebugEnabled())
            myLogger.debug("Decrypted value  = " + value);
        return value;
    }
	
	public static String guessDomain(HttpServletRequest request){
		String serverName = ".scholastic.com";
		
		if (request!=null && request.getServerName()!=null){
			/* TODO : remove it when dev phase is over.
			if(request.getServerName().indexOf(".net") != -1)
				return ".scholastic.net";
			else if(request.getServerName().indexOf(".com") != -1)
				return ".scholastic.com";
			*/
			serverName = request.getServerName();
			if (serverName.indexOf('.') != -1 ) {
				serverName = serverName.substring(serverName.indexOf('.'));
			}
		}
		if(myLogger.isDebugEnabled())  myLogger.debug("SPS Cookie Domain   = " + serverName);
		return serverName;
	}
	
}
