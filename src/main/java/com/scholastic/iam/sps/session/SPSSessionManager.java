package com.scholastic.iam.sps.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.scholastic.iam.sps.SPSException;
import com.scholastic.iam.sps.service.UserService;
import com.scholastic.iam.sps.session.SPSCookieHelper;


public class SPSSessionManager {

	private static Logger myLogger = Logger.getLogger("session");
	
	public static final String SPS_SESSION_COOKIE_NAME = "SPS_SESSION";
	public static final String IE_SUX_COOKIE_NAME = "XUS_EI";
	public static final String SPS_TSTAMP_COOKIE_NAME = "SPS_TSP";
	public static final String SPS_USER_DATA_COOKIE_NAME = "SPS_UD";
	
	public static final String WOI_SPS_SESSION_COOKIE_NAME = "WOI_SPS_SESSION";
	public static final String WOI_IE_SUX_COOKIE_NAME = "WOI_XUS_EI";
	public static final String WOI_SPS_TSTAMP_COOKIE_NAME = "WOI_SPS_TSP";
	public static final String WOI_SPS_USER_DATA_COOKIE_NAME = "WOI_SPS_UD";
	private static final String BUS_COOKIE_NAME = "SPS_BUS";
	public final static String EDUREGCONFIRMATION = "Omni_EducatorRegistration";
	public final static String NONEDUREGCONFIRMATION = "Omni_ConsumerRegistration";
	public final static String ORGBORDERID = "orgBorderId";
	
	private static String JBSKey = null;

	public static long MAX_AGE = 30 * 60 * 1000;
	public static int MAX_AGE_20_YEARS = 20 * 365 * 24 * 60 * 60;

	private static final UserService userService = new UserService();
	
	public static boolean isLoggedIn(String sessionValue, String tStamp) {
		if (StringUtils.isEmpty(sessionValue)) {
			if (myLogger.isDebugEnabled())
				myLogger.debug("Not logged in: invalid SessionValue");
			return false;
		}

		if (!StringUtils.isNumeric(tStamp)) {
			if (myLogger.isDebugEnabled())
				myLogger.debug("Not logged in: TS is not a number!!");
			return false;
		}

		
		
		long loginTime = new Long(tStamp).longValue();
		long currTime = System.currentTimeMillis();

		if (currTime > loginTime) {
			if (myLogger.isDebugEnabled())
				myLogger
						.debug("Not logged in: " + currTime + " > " + loginTime);
			return false;
		} else {
			if (myLogger.isDebugEnabled())
				myLogger.debug("YES,  logged in: " + currTime + " < "
						+ loginTime);
		}

		return true;
	}
	
		//USED TO START THE SESSION
	public static boolean loginUser(String spsId, HttpServletRequest request, HttpServletResponse response,boolean fromWOI) {
		myLogger.debug("SPsSessionManager loginUser fromWOI..."+fromWOI);
		if(fromWOI){
			SPSCookieHelper.placeCookie(request, response, WOI_SPS_SESSION_COOKIE_NAME, spsId, false, true);
			SPSCookieHelper.placeCookie(request, response, WOI_IE_SUX_COOKIE_NAME, spsId, true, true);	
		}else{
			SPSCookieHelper.placeCookie(request, response, SPS_SESSION_COOKIE_NAME, spsId, false, true);
			SPSCookieHelper.placeCookie(request, response, IE_SUX_COOKIE_NAME, spsId, true, true);	
		}
				
		if(fromWOI){
			return extendSession(response, request,fromWOI);	
		}else{
			return extendSession(response, request);
		}
		
	}
	
	//USED TO CHECK IF THERE IS A VALID SESSION
	public static boolean isLoggedIn(HttpServletRequest request) {
		return isLoggedIn(SPSCookieHelper.getCookieValue(request, SPS_SESSION_COOKIE_NAME, true), getTStampValue(request,false));
	}

	public static boolean placeUDCookie(HttpServletRequest request, HttpServletResponse response, SpsUser spsUser){
		//return SPSCookieHelper.placeCookie(request,response,SPS_USER_DATA_COOKIE_NAME, generateUDCookieValue(obj), MAX_AGE_20_YEARS, false);
		return placeUDCookie(request,response, spsUser, false);
	}
	
	public static boolean placeUDCookie(HttpServletRequest request, HttpServletResponse response, SpsUser spsUser,boolean fromWOI){
		myLogger.debug("placeUDCookie.... fromWOI :"+fromWOI);
		return (fromWOI) ? SPSCookieHelper.placeCookie(request,response,WOI_SPS_USER_DATA_COOKIE_NAME,generateUDCookieValue(spsUser), MAX_AGE_20_YEARS, false)
						 : SPSCookieHelper.placeCookie(request,response,SPS_USER_DATA_COOKIE_NAME,generateUDCookieValue(spsUser), MAX_AGE_20_YEARS, false);
	}
	
	
	public static String getSpsIdBySession(HttpServletRequest request, HttpServletResponse response) {
		if (response == null) {
			if (!isLoggedIn(request))
				return null;
		} else {
			if (!isLoggedIn(request, response))
				return null;
		}
		return SPSCookieHelper.getCookieValue(request, SPS_SESSION_COOKIE_NAME, true);
	}
	
	public static SpsUser getUserBySession(HttpServletRequest request, HttpServletResponse response) throws SPSException{
		if(myLogger.isDebugEnabled())
			myLogger.debug("In getUserBySession started ");
		
		String spsID = SPSSessionManager.getSpsIdBySession(request, response);
		if(myLogger.isDebugEnabled())
			myLogger.debug("In getUserBySession spsID = SPSSessionManager.getSpsIdBySession(request, response)== " + spsID + " ==");
		
		if(StringUtils.isEmpty(spsID))
			return null;
		
		try{
			SpsUser user = (SpsUser)request.getSession().getAttribute("USER_IN_REQUEST");
			if(user != null) {
				if(myLogger.isDebugEnabled())
					myLogger.debug("In getUserBySession spsID from the SPSUser.getSpsId() from REQUEST == " + user.getSpsId() + " ==");
				if(spsID.equals(user.getSpsId())) {
					if(myLogger.isDebugEnabled())
						myLogger.debug("SPS ID MATCHED");
					return user;
				}
			}
		}catch(Exception ignored){}
		if(myLogger.isDebugEnabled())
			myLogger.debug("Before creating new spsUser from LDAP spsID from session is ==" + spsID + "==");
		
		SpsUser user = null;
		try {
			user = userService.getUserBySpsId(spsID);
		}
		catch (Exception se) {
			throw new SPSException("Error lookup up user " + spsID, se);
		}
		
		if (user == null) {			
			if(myLogger.isDebugEnabled()) {
				myLogger.debug("Unable to locate user with spsid = " + spsID);
			}
			return null;
		}
		if(myLogger.isDebugEnabled())
			myLogger.debug("In getUserBySession spsID from the SPSUser.getSpsId() from REQUEST == " + user.getSpsId() + " ==");

		request.getSession().setAttribute("USER_IN_REQUEST", user);
		if(myLogger.isDebugEnabled())
			myLogger.debug("NEWLY CREATED USER PUT IN REQUEST");

		return user;
	}
	
	
	public static String generateUDCookieValue(SpsUser spsUser){
		String value = spsUser.getSpsId() +"|"+
			spsUser.getEmail() +"|"+
			spsUser.getFirstName() +"|"+
			spsUser.getLastName() +"|"+
			spsUser.getBcoeId();
			
		if (SPSSessionManager.myLogger.isDebugEnabled())
			SPSSessionManager.myLogger.debug(SPS_USER_DATA_COOKIE_NAME + " value: " + value);
		
		return value;
	}
	
	public static boolean extendSession(HttpServletResponse response, HttpServletRequest request,boolean fromWOI) {
		if(response == null)
			return false;
		String value = "" + (System.currentTimeMillis() + MAX_AGE);
		
		if(fromWOI){
			SPSCookieHelper.placeCookie(request, response, WOI_SPS_TSTAMP_COOKIE_NAME, value, true, true);
		}else{
			SPSCookieHelper.placeCookie(request, response, SPS_TSTAMP_COOKIE_NAME, value, true, true);	
		}

		return true;
	}
	
	public static boolean extendSession(HttpServletResponse response, HttpServletRequest request) {
		/*if(response == null)
			return false;
		String value = "" + (System.currentTimeMillis() + MAX_AGE);
		SPSCookieHelper.placeCookie(request, response, SPS_TSTAMP_COOKIE_NAME, value, true, true);
		//SPSLithiumSSO.extendLithiumSession(request, response);
		//SPSPrefCtrSSO.extendPrefCtrSession(request, response);
		return true;*/
		return extendSession(response,request,false);
	}
	
	
	
	public static boolean isLoggedIn(HttpServletRequest request, HttpServletResponse response) {
		if (!isLoggedIn(request))
			//check for openam session session , and create sps session if openam session exists
			return false;
		return 
				 //extend openam session every 5 minutes, it openam session exists - nice to have
				 //this is just only to avoid delays on openam side if openam does not exist as it has to 
				 //recreate openam session 
				 extendSession(response, request);
	}
	
	protected static String getTStampValue(HttpServletRequest request,boolean fromWOI) {
		String value = null;
		value = (fromWOI) ? SPSCookieHelper.getCookieValue(request,WOI_SPS_TSTAMP_COOKIE_NAME, true)
						  : SPSCookieHelper.getCookieValue(request,SPS_TSTAMP_COOKIE_NAME, true);
		if (myLogger.isDebugEnabled())
			myLogger.debug(SPS_TSTAMP_COOKIE_NAME + " value: " + value);
			myLogger.debug(WOI_SPS_TSTAMP_COOKIE_NAME + " value: " + value);
		return value;
	}

	
}
