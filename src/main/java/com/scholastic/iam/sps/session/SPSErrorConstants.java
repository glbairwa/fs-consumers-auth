//
// SPSErrorConstants created by alexpod on  Jun 22, 2004
// Modified on 06/01/2012 by Venkat Medarametla.
//
 
package com.scholastic.iam.sps.session;


public interface SPSErrorConstants {
//	error  types
	public static final int DEFAULT_ERROR = 0;
	public static final int OPERATION_NOT_PERMITED = 1;
	public static final int RESOURCE_FULL = 2;
	public static final int FAILED_TO_CONNECT = 3;
	public static final int OPERATION_TIMEOUT = 4;
	public static final int INVALID_DATASOURCE_ID = 5;
	public static final int RESOURCE_NOT_READY = 6;
	public static final int OPERATION_FAILED = 7;
	public static final int SIZE_LIMIT_EXCEEDED = 8;
	public static final int DUPLICATE_SPSID = 9;
	public static final int INVALID_SPSID = 10;	
	public static final int INVALID_PARAMS = 11;
	public static final int UNAUTHORIZED_OPERATION = 12;	
	public static final int SHUTDOWN_IN_PROCESS = 110;	
	public static final int DROP_BATCH = 111;
	
}
