package com.scholastic.iam.sps.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;



public class SPSEnvProperties {
	//make sure the directories ends with /  and the path is absolute.
	private static final String [] DEFAULT_OVERRIDE_DIRS  = {"/var/webapp/config/"};
	private static SPSEnvProperties _instance = getInstance();
	private Logger log = Logger.getLogger(SPSEnvProperties.class);
	
    public static SPSEnvProperties  getInstance() {
    	if ( _instance == null ) {
    		synchronized(SPSEnvProperties.class){
    			_instance = new SPSEnvProperties();
    		}
    	}
    	return _instance;    	
    }
	
    
    private SPSEnvProperties() {
    	props = new Properties();
		try {
			props.load(SPSEnvProperties.class.getResourceAsStream("/spsenv.properties"));
			//override with files in override directories
			for ( String dir:DEFAULT_OVERRIDE_DIRS  ) {		
				try {
					InputStream propFileIS = new FileInputStream(dir+"spsenv.properties");
					if ( propFileIS != null )  { 
						props.load(propFileIS);		
						log.debug("Loaded property file:  "+dir+"spsenv.properties");
					}
				}catch ( FileNotFoundException fnfe ) {					
				}
			}			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("ERROR: Could not read spsenv.properties ", e);
		}
    }
    
    public String getString(String key) { 
    	return props != null ? props.getProperty(key): null;
    }

    public int  getInt(String key) {     	
    	return props != null ? Integer.parseInt(props.getProperty(key)): null;
    }
    
    private Properties props = null;
    
    
}
