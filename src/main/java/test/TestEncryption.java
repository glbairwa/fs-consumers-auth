package test;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.scholastic.iam.sps.util.Base64;

public class TestEncryption {

	public TestEncryption() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws Exception {
		Cipher cipher = Cipher.getInstance("Blowfish/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(
				"INTERNAL_key88".getBytes(), "Blowfish"));
		String actualKey = new String(cipher.doFinal(Base64.decode("0rfLX5x3SNHM1epE9vjJoA==" + '\n')));
		System.out.println("actualKey = " + actualKey);
		
//		
		SecretKeySpec secretKeySpec = new SecretKeySpec(actualKey.getBytes(), "Blowfish");
		Cipher decryptCipher = Cipher.getInstance("Blowfish/ECB/PKCS5Padding");
		decryptCipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
//		String decryptedString = new String(decryptCipher.doFinal(Base64.decode("E38Su5erMfU=")));
		String userId = new String(decryptCipher.doFinal(Base64.decode("+q2ExTYNGQufUOpAZpgacA==")));
		System.out.println("userId = " + userId);
		
		String timestamp = new String(decryptCipher.doFinal(Base64.decode("bawgpoDjtPSGSYNH9eh/sA==")));
		System.out.println("timestamp = " + timestamp);

//		
//		providerProps.key = new String(cipher.doFinal(Base64.decode(providerProps.key.trim() + '\n')));		
		// TODO Auto-generated method stub
//		String pw = SchBlowfishCodec.singleton().decodeText("E38Su5erMfU=");
//		System.out.println("Found user nancykelley, pw=" + pw);
		
	}

}
